<?php
//Verbinding maken met de database
require_once 'db.php';
$mysqli =  connectDB();
$query="SELECT username FROM users";
include ('security.php');
if ($_SESSION['role']!='admin') {
	header("HTTP/1.1 403 Unauthorized");
	header("Location: 403.php");
	exit;
}
?>

<!DOCTYPE html>

<html lang = "en">
	<head>
		<title>user management</title>
		<?php include 'head.html'
		?>
	</head>
	<body>
		<?php include 'header.php';
		?>
		<form method= "post" action="usermanage.php">
			<td>
			<tr>
				<select name="username"><label for="username">
					<?php
					$resultusers = $mysqli -> query($query);
					if ($resultusers -> num_rows > 0) {
						while ($users = $resultusers -> fetch_assoc()) {
							echo "<option value=" . $users['username'] . ">" . $users['username'] . "</option>";

						}
					}
					?>
					</label>
				</select>
				<select name="role"><label for="role">
					<option value="speler" name="speler" id="speler"> Speler </option>
					<option value="coach" name="coach" id="coach"> Coach </option>
					<option value="admin" name="admin" id="admin"> Admin </option>
					<option value="delete" name="delete" id="delete"> Verwijderen </option>
					</label>
				</select>
		<table>
			<tr>
				<td colspan="2" style="text-align:center">
				<input type="submit" value="Verstuur">
				</td>
			</tr>
		</table> 
		</form>
	</body>
</html>

