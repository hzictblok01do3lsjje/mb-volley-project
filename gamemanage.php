<?php
require_once 'db.php';
$mysqli = connectDB();
include ('security.php');
if ($_SESSION['role']!='admin') {
	if ($_SESSION['role']!='coach') {
		header("HTTP/1.1 403 Unauthorized");
		header("Location: 403.php");
		exit;
	}
}

if(isset($_POST['wedstrijdid'])){
	$wedstrijdid= $_POST['wedstrijdid'];
}

if(isset($_GET['wedstrijdid'])){
	$wedstrijdid= $_GET['wedstrijdid'];
}

if(isset($_POST['klas']) ){
	$klas=$_POST['klas'];
	$query1 = "SELECT naam,id FROM team WHERE klasse ='".$klas."' ;";
	$query2 = "SELECT naam,id FROM team WHERE klasse !='".$klas."' ;";
}else{
	$klas="H1";
	$query1 = "SELECT naam,id FROM team WHERE klasse ='".$klas."' ;";
	$query2 = "SELECT naam,id FROM team WHERE klasse !='".$klas."' ;";
}
include 'head.html';
?>

<!DOCTYPE html>

<html lang = "en">
	<head>
		<title>Registratie</title>
		<?php include 'head.html'
		?>
	</head>
	<body>
		<?php include 'header.php'
		?>
		<form method= "post" action="addgame.php">
			<div class="gamemanage">
				<table>
					<td>
					<tr>
						<select name="klas" onchange="this.form.submit()">
							<option value="H1" <?php if($klas=="H1"){echo "selected";} ?>>H1</option>
							<option value="H2" <?php if($klas=="H2"){echo "selected";} ?>>H2</option>
							<option value="H3" <?php if($klas=="H3"){echo "selected";} ?>>H3</option>
							<option value= "D1" <?php if($klas=="D1"){echo "selected";} ?>>D1</option>
							<option value="D2" <?php if($klas=="D2"){echo "selected";} ?>>D2</option>
						</select>
						<select name="teama" ">
							<?php
							$resultteam = $mysqli -> query($query1);
							if ($resultteam -> num_rows > 0) {
								while ($team = $resultteam -> fetch_assoc()) {
									echo "<option value=" . $team['id'] . ">" . $team['naam'] . "</option>";

								}
							}
							?>
						</select>
						<select name="teamb">
							<?php
							$resultteam = $mysqli -> query($query1);
							if ($resultteam -> num_rows > 0) {
								while ($team = $resultteam -> fetch_assoc()) {
									echo "<option value=" . $team['id'] . ">" . $team['naam'] . "</option>";

								}
							}
							?>
						</select>
						</select>
						<select name="scheidsrechter">
							<?php
							$resultteam = $mysqli -> query($query2);
							if ($resultteam -> num_rows > 0) {
								while ($team = $resultteam -> fetch_assoc()) {
									echo "<option value=" . $team['id'] . ">" . $team['naam'] . "</option>";

								}
							}
							?>
						</select>
					</tr></td>
				</table>
				<input type="hidden" name="wedstrijdid" value="<?php echo $wedstrijdid ?>"/>
				<input name="submit" type="submit" value="Toevoegen">
			</div>
		</form>
	</body>
</html>

