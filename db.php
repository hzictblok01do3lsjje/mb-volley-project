<?php

	function connectDB() {
		$url = 'p:localhost';
		$userid = 'mbvolley';
		$password = 'mbvolley';
		$database = 'mbvolley';
	    $mysqli = new mysqli($url, $userid, $password, $database);
		if ($mysqli->connect_errno) {
	    	return "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		}
		return $mysqli;
	}
?>