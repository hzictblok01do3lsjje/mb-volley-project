<?php
// Sessions, Security and Authorization
include ('security.php');
if ($_SESSION['role']!='admin') {
	if($_SESSION['role']!='coach'){
	header("HTTP/1.1 403 Unauthorized");
	header("Location: 403.php");
	exit;
	}
}
	
//Verbinding maken met de database
	require_once 'db.php';
	$mysqli =  connectDB();


	if ($_SERVER['REQUEST_METHOD']=='POST') {
		// even gemakkelijk, zonder validatie of andere checks
		// PAS OP!!! Gevaarlijk voor SQL-injectie of andere aanvallen en foutgevoelig
		$wedstrijdid = $_POST['wedstrijdid'];

		// Wie is team A?
		$sql = "SELECT * FROM WEDSTRIJD_VIEW WHERE id =".$wedstrijdid;
		$result = $mysqli->query($sql);
		if($result->num_rows == 0) {
			die("Wedstrijd wedstrijdid is niet bekend");
		}
		$wedstrijddata = $result->fetch_assoc();		
		if($wedstrijddata['uitslag']==1){
			$sql = "DELETE FROM `UITSLAG_SET_TEAM` WHERE wedstrijd='".$wedstrijdid."'";
			$result = $mysqli->query($sql);
		};
		
		for ($set=1; $set<=4; $set++){
			if (strlen($_POST['S'.$set.'Sa'])>0 && 
			
				strlen($_POST['S'.$set.'Sa'])>0 && 
				strlen($_POST['S'.$set.'Sa'])>0 && 
				strlen($_POST['S'.$set.'Sa'])>0) {
				// Score en punten team a
				$sql  = "INSERT INTO UITSLAG_SET_TEAM(wedstrijd, `set`, team, score, punten) VALUES(";
				$sql .= $wedstrijdid.", ";
				$sql .= $set.", ";
				$sql .= $wedstrijddata['taid'].", ";
				$sql .= $_POST['S'.$set.'Sa'].", ";
				$sql .= $_POST['S'.$set.'Pa'].")";
				$result = $mysqli->query($sql);
				// Score en punten team b
				$sql  = "INSERT INTO UITSLAG_SET_TEAM(wedstrijd, `set`, team, score, punten) VALUES(";
				$sql .= $wedstrijdid.", ";
				$sql .= $set.", ";
				$sql .= $wedstrijddata['tbid'].", ";
				$sql .= $_POST['S'.$set.'Sb'].", ";
				$sql .= $_POST['S'.$set.'Pb'].")";
				$result = $mysqli->query($sql);

				header("location:uitslagen.php");
			}
		}
		exit;
	}

	$teamid = 0;
	if(isset($_GET['wedstrijdid'])) {
		$wedstrijdid = $_GET['wedstrijdid'];
		$sql = "SELECT * FROM WEDSTRIJD_VIEW WHERE id=$wedstrijdid";
		$result = $mysqli->query($sql);
		if($result->num_rows >0) {
			$wedstrijddata = $result->fetch_assoc();
		}

	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well"><h1>Invullen uitslag</h1></div>
			<form method="POST">
				<div class="panel panel-default">
					<div class="panel-heading">
							<div class="row">
								<div class="col-xs-4"><strong>DATUM:</strong> <?php echo $wedstrijddata['datum'] ?></div>
								<div class="col-xs-4"><strong>TIJD:</strong> <?php echo $wedstrijddata['tijd'] ?></div>
								<div class="col-xs-2"><strong>VELD:</strong> <?php echo $wedstrijddata['veld'] ?></div>
								<div class="col-xs-2"><strong>KLAS:</strong> <?php echo $wedstrijddata['klasse'] ?></div>
							</div>
							<br/>
							<div class="row">
								<div class="col-xs-12"><strong>SCHEIDSRECHTER:</strong> <?php echo $wedstrijddata['teams'] ?></div>
							</div>
							<br/>
							<div class="row">
								<div class="col-xs-6"><strong>TEAM A:</strong> <?php echo $wedstrijddata['teama'] ?></div>
								<div class="col-xs-6"><strong>TEAM B:</strong> <?php echo $wedstrijddata['teamb'] ?></div>
							</div>
					</div>
					<div class="panel-body">
						<input type="hidden" name="wedstrijdid" value="<?php echo $wedstrijddata['id'] ?>" />
						<table class="table table-striped">
							<thead>
								<tr>
									<th rowspan="2">SET</th>
									<th colspan="2">SCORE</th>
									<th colspan="2">PUNTEN</th>
								</tr>
								<tr>
									<th>TEAM A</th>
									<th>TEAM B</th>
									<th>TEAM A</th>
									<th>TEAM B</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th><strong>1</strong></th>
									<th><input type="text" class="form-control" name="S1Sa" placeholder="score"></th>
									<th><input type="text" class="form-control" name="S1Sb" placeholder="score"></th>
									<th><input type="text" class="form-control" name="S1Pa" placeholder="punten"></th>
									<th><input type="text" class="form-control" name="S1Pb" placeholder="punten"></th>
								</tr>
								<tr>
									<th><strong>2</strong></th>
									<th><input type="text" class="form-control" name="S2Sa" placeholder="score"></th>
									<th><input type="text" class="form-control" name="S2Sb" placeholder="score"></th>
									<th><input type="text" class="form-control" name="S2Pa" placeholder="punten"></th>
									<th><input type="text" class="form-control" name="S2Pb" placeholder="punten"></th>
								</tr>
								<tr>
									<th><strong>3</strong></th>
									<th><input type="text" class="form-control" name="S3Sa" placeholder="score"></th>
									<th><input type="text" class="form-control" name="S3Sb" placeholder="score"></th>
									<th><input type="text" class="form-control" name="S3Pa" placeholder="punten"></th>
									<th><input type="text" class="form-control" name="S3Pb" placeholder="punten"></th>
								</tr>
								<tr>
									<th><strong>4</strong></th>
									<th><input type="text" class="form-control" name="S4Sa" placeholder="score"></th>
									<th><input type="text" class="form-control" name="S4Sb" placeholder="score"></th>
									<th><input type="text" class="form-control" name="S4Pa" placeholder="punten"></th>
									<th><input type="text" class="form-control" name="S4Pb" placeholder="punten"></th>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-primary">Opslaan</button>
					</div>
				</div>
			</form>
		</main>
	</body>
</html>
 