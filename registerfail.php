<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Registratie</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well">
				<h1>
					Het registreren van je account is mislukt, terug naar registreren....
				</h1>
				<p>
					Weet je zeker dat je alles correct hebt ingevoerd en bijvoorbeeld geen letters bij je telefoonnummer?
				</p>
				<button onclick= "location.href='register.php'">Terug</button>
			</div>
		</main>
	</body>
</html>
