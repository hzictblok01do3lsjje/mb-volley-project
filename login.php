<?php
// Sessions, Security and Authorization
include ('security.php');

	// controleer of uitloggen
	if (isset($_GET['action']) && $_GET['action']==='logoff') {
		session_destroy();
		//Een header sturen
		header('Location: index.php');
		exit;
	};
	$loginTry = isset($_POST['username']) && isset($_POST['password']);
	// Controleer op inloggen 
	if ($loginTry) {
		include_once('userstorage.php');
		$username = strip_tags($_POST['username']);
		$password = strip_tags($_POST['password']);
		if (authenticateUser($username, $password)) {
			//Een header sturen
			header('Location: index.php');
			exit;
		}
	}
	
		
	// anders form laten zien
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<?php
			if ($loginTry && !isAuthenticated()) {
				echo '<span class="error">Login niet succesvol. Probeer opnieuw</span>';
			}
			?>
			<form method="post" action="login.php" style="width: 450px; margin-left: auto; margin-right: auto">
				<table >
					<tr>
						 <td valign="top">
						  	<label for="username">Gebruikersnaam</label>
						 </td>
						 <td valign="top">
						  	<input  type="text" name="username" maxlength="50" size="30">
						 </td>
					</tr>
					<tr>
						 <td valign="top">
						  	<label for="password">Wachtwoord</label>
						 </td>
						 <td valign="top">
						  	<input  type="password" name="password" value="" maxlength="50" size="30">
						 </td>
					</tr>
					<tr>
						 <td valign="top"></td>
						 <td valign="top">
						  	<input type="submit" name="commit" value="Login">
						 </td>
					</tr>
				</table>	
			</form>
		</main>
	</body>
</html>
