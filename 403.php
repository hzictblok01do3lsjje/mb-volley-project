<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBVolley - 403 Unauthorized</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<main class="container">
			<div class="jumbotron">	
				<h1>403 Unauthorized</h1>
				<div class="alert alert-danger" role="alert">
					<i class="fa fa-times-circle fa-5x"></i>You are not authorized to see this page.
				</div>
			</div>	
		</main>
	</body>
</html>
 