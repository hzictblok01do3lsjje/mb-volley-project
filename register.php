<?php
// Sessions, Security
 include ('security.php');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">
			<?php
			// Externe scripts includen 	 
			require_once('userstorage.php');
			include_once('formvalidationtools.php');
			
			// Controleer of form wordt opgevraagd of ingestuurd
		    if (isset($_POST['send'])) {
		    	// Form ingestuurd: Verwerken maar
		    	
			    // Als eerste stap controleren en valideren we alle data uit het form
			    if(!isset($_POST['username']) ||
			        !isset($_POST['name_first']) ||
			        !isset($_POST['name_last']) ||
			        !isset($_POST['sex']) ||
			        !isset($_POST['address_street']) ||
			        !isset($_POST['address_city']) ||
			        !isset($_POST['address_zipcode']) ||
			        !isset($_POST['address_country']) ||
			        !isset($_POST['phone']) ||
			        !isset($_POST['email']) ||
			        !isset($_POST['password'])) {
			        printErrorAndDie('Het lijkt er op dat het formulier dat u gebruikt niet klopt.');       
			    }
					
			    // Overnemen van de dat uit het form naar lokale variabelen 
				$username = strip_tags($_POST['username']);
				$name_first = strip_tags($_POST['name_first']);
				$name_last = strip_tags($_POST['name_last']);
				$sex = strip_tags($_POST['sex']);
				$address_street = strip_tags($_POST['address_street']);
				$address_city = strip_tags($_POST['address_city']);
				$address_zipcode = strip_tags($_POST['address_zipcode']);
				$address_country = strip_tags($_POST['address_country']);
				$phone = strip_tags($_POST['phone']);
				$email = strip_tags($_POST['email']);
				$password = strip_tags($_POST['password']);
		
			    // Valideren van de input, met behulp van extern script met functies
			    // error_message wordt gevuld als er foutberichten zijn 
			    $error_message = "";
				$error_message .= validateCharacters($username, 'De voornaam is niet valide.');
				// Extra validatie: kijk of usernaam al bestaat
				if (usernameExists($username)) {
					$error_message .= 'De usernaam bestaat al.<br />';			
				}		
				$error_message .= validateCharacters($name_first, 'De voornaam is niet valide.');
				$error_message .= validateCharacters($name_last, 'De achternaam is niet valide.');
				$error_message .= validateCharacters($address_street, 'De straatnaam is niet ingevuld.');
				$error_message .= validateCharacters($address_city, 'De stad is niet ingevuld.');
				$error_message .= validateLength($address_zipcode, 1, 'De postcode is niet ingevuld.');
				$error_message .= validateLength($address_country, 1, 'Het land is niet ingevuld.');
				$error_message .= validateLength($phone, 1, 'Het telefoonnummer is niet ingevuld.');
				$error_message .= validateEmail($email, 'Het email adres is niet valide');
				$error_message .= validateLength($password, 4, 'Het wachtwoord is te kort.');
				// Er is iets mis als de lengte van error_message > 0
				if(strlen($error_message) > 0) {
				    printErrorAndDie($error_message);
				}
				
				// De input is nu goed, dus kan het worden verwerkt
				addUser($username, $password, $name_first, $name_last, $sex, $phone, $email, $address_street, $address_city, 
						$address_zipcode, $address_country);
						
				//Een header sturen
				header('Location: index.php');
				exit;
								
		    } else {
		    	// Het form laten zien...

		    			    	?><form action="insertusers.php" name="input" id="registerform" method="post" style="width: 850px; margin-left: auto; margin-right: auto">
		    			    		
				      
					<h1>Registreer Nieuwe Gebruiker</h1>
					<p>Vul dit formulier in om een nieuwe gebuiker in te kunnen laten loggen op de site</p>
					<input type="hidden" name="send" value="true"/>
					<table width="850px">
						<tr>
							 <td width="230px"><label for="username">Gebruikersnaam *</label></td>
							 <td width="265px"><input class- type="text" id="username" name="username" maxlength="50" size="30"></td>
							 <td><span id="usernameValResult"> </span></td>
						</tr>		 
						<tr>
							 <td width="230px"><label for="name_first">Voornaam *</label></td>
							 <td width="265px"><input  type="text" id="name_first" name="name_first" maxlength="50" size="30"></td>
							 <td><span id="name_firstValResult"> </span></td>
						</tr>		 
						<tr>
							 <td><label for="name_last">Achternaam *</label></td>
							 <td><input  type="text" id="name_last" name="name_last" maxlength="50" size="30"></td>
							 <td><span id="name_lastValResult"> </span></td>
						</tr>
						<tr>
							<td><label for="sex"><strong>Geslacht</strong></label></td>
							<td>
								<input type="radio" name="sex" value="male">Man<br>
								<input type="radio" name="sex" value="female">Vrouw
							</td>
							 <td><span id="sexValResult"> </span></td>
						</tr>
						<tr>
							 <td><label for="address_street">Adres straat en huisnummer *</label></td>
							 <td><input  type="text" id="address_street" name="address_street" maxlength="50" size="30"></td>
							 <td><span id="address_streetValResult"> </span></td>
						</tr>
						<tr>
							 <td><label for="address_city">Adres plaatsnaam *</label></td>
							 <td><input  type="text" id="address_city" name="address_city" maxlength="50" size="30"></td>
							 <td><span id="address_cityValResult"> </span></td>
						</tr>
						<tr>
							 <td><label for="address_zipcode">Adres postcode *</label></td>
							 <td><input  type="text" id="address_zipcode" name="address_zipcode" maxlength="50" size="30"></td>
							 <td><span id="address_zipcodeValResult"> </span></td>
						</tr>
						<tr>
							 <td><label for="address_country">Adres land *</label></td>
							 <td>
							 	<select id="address_country" name="address_country"> 
									<option value="" selected="selected"></option>
									<option value="Afghanistan" >Afghanistan</option>
									<option value="Albania" >Albania</option>
									<option value="Algeria" >Algeria</option>
									<option value="Andorra" >Andorra</option>
									<option value="Antigua and Barbuda" >Antigua and Barbuda</option>
									<option value="Argentina" >Argentina</option>
									<option value="Armenia" >Armenia</option>
									<option value="Australia" >Australia</option>
									<option value="Austria" >Austria</option>
									<option value="Azerbaijan" >Azerbaijan</option>
									<option value="Bahamas" >Bahamas</option>
									<option value="Bahrain" >Bahrain</option>
									<option value="Bangladesh" >Bangladesh</option>
									<option value="Barbados" >Barbados</option>
									<option value="Belarus" >Belarus</option>
									<option value="Belgium" >Belgium</option>
									<option value="Belize" >Belize</option>
									<option value="Benin" >Benin</option>
									<option value="Bhutan" >Bhutan</option>
									<option value="Bolivia" >Bolivia</option>
									<option value="Bosnia and Herzegovina" >Bosnia and Herzegovina</option>
									<option value="Botswana" >Botswana</option>
									<option value="Brazil" >Brazil</option>
									<option value="Brunei" >Brunei</option>
									<option value="Bulgaria" >Bulgaria</option>
									<option value="Burkina Faso" >Burkina Faso</option>
									<option value="Burundi" >Burundi</option>
									<option value="Cambodia" >Cambodia</option>
									<option value="Cameroon" >Cameroon</option>
									<option value="Canada" >Canada</option>
									<option value="Cape Verde" >Cape Verde</option>
									<option value="Central African Republic" >Central African Republic</option>
									<option value="Chad" >Chad</option>
									<option value="Chile" >Chile</option>
									<option value="China" >China</option>
									<option value="Colombia" >Colombia</option>
									<option value="Comoros" >Comoros</option>
									<option value="Congo" >Congo</option>
									<option value="Costa Rica" >Costa Rica</option>
									<option value="CÃ´te d'Ivoire" >CÃ´te d'Ivoire</option>
									<option value="Croatia" >Croatia</option>
									<option value="Cuba" >Cuba</option>
									<option value="Cyprus" >Cyprus</option>
									<option value="Czech Republic" >Czech Republic</option>
									<option value="Denmark" >Denmark</option>
									<option value="Djibouti" >Djibouti</option>
									<option value="Dominica" >Dominica</option>
									<option value="Dominican Republic" >Dominican Republic</option>
									<option value="East Timor" >East Timor</option>
									<option value="Ecuador" >Ecuador</option>
									<option value="Egypt" >Egypt</option>
									<option value="El Salvador" >El Salvador</option>
									<option value="Equatorial Guinea" >Equatorial Guinea</option>
									<option value="Eritrea" >Eritrea</option>
									<option value="Estonia" >Estonia</option>
									<option value="Ethiopia" >Ethiopia</option>
									<option value="Fiji" >Fiji</option>
									<option value="Finland" >Finland</option>
									<option value="France" >France</option>
									<option value="Gabon" >Gabon</option>
									<option value="Gambia" >Gambia</option>
									<option value="Georgia" >Georgia</option>
									<option value="Germany" >Germany</option>
									<option value="Ghana" >Ghana</option>
									<option value="Greece" >Greece</option>
									<option value="Grenada" >Grenada</option>
									<option value="Guatemala" >Guatemala</option>
									<option value="Guinea" >Guinea</option>
									<option value="Guinea-Bissau" >Guinea-Bissau</option>
									<option value="Guyana" >Guyana</option>
									<option value="Haiti" >Haiti</option>
									<option value="Honduras" >Honduras</option>
									<option value="Hong Kong" >Hong Kong</option>
									<option value="Hungary" >Hungary</option>
									<option value="Iceland" >Iceland</option>
									<option value="India" >India</option>
									<option value="Indonesia" >Indonesia</option>
									<option value="Iran" >Iran</option>
									<option value="Iraq" >Iraq</option>
									<option value="Ireland" >Ireland</option>
									<option value="Israel" >Israel</option>
									<option value="Italy" >Italy</option>
									<option value="Jamaica" >Jamaica</option>
									<option value="Japan" >Japan</option>
									<option value="Jordan" >Jordan</option>
									<option value="Kazakhstan" >Kazakhstan</option>
									<option value="Kenya" >Kenya</option>
									<option value="Kiribati" >Kiribati</option>
									<option value="North Korea" >North Korea</option>
									<option value="South Korea" >South Korea</option>
									<option value="Kuwait" >Kuwait</option>
									<option value="Kyrgyzstan" >Kyrgyzstan</option>
									<option value="Laos" >Laos</option>
									<option value="Latvia" >Latvia</option>
									<option value="Lebanon" >Lebanon</option>
									<option value="Lesotho" >Lesotho</option>
									<option value="Liberia" >Liberia</option>
									<option value="Libya" >Libya</option>
									<option value="Liechtenstein" >Liechtenstein</option>
									<option value="Lithuania" >Lithuania</option>
									<option value="Luxembourg" >Luxembourg</option>
									<option value="Macedonia" >Macedonia</option>
									<option value="Madagascar" >Madagascar</option>
									<option value="Malawi" >Malawi</option>
									<option value="Malaysia" >Malaysia</option>
									<option value="Maldives" >Maldives</option>
									<option value="Mali" >Mali</option>
									<option value="Malta" >Malta</option>
									<option value="Marshall Islands" >Marshall Islands</option>
									<option value="Mauritania" >Mauritania</option>
									<option value="Mauritius" >Mauritius</option>
									<option value="Mexico" >Mexico</option>
									<option value="Micronesia" >Micronesia</option>
									<option value="Moldova" >Moldova</option>
									<option value="Monaco" >Monaco</option>
									<option value="Mongolia" >Mongolia</option>
									<option value="Montenegro" >Montenegro</option>
									<option value="Morocco" >Morocco</option>
									<option value="Mozambique" >Mozambique</option>
									<option value="Myanmar" >Myanmar</option>
									<option value="Namibia" >Namibia</option>
									<option value="Nauru" >Nauru</option>
									<option value="Nepal" >Nepal</option>
									<option value="Netherlands" >Netherlands</option>
									<option value="New Zealand" >New Zealand</option>
									<option value="Nicaragua" >Nicaragua</option>
									<option value="Niger" >Niger</option>
									<option value="Nigeria" >Nigeria</option>
									<option value="Norway" >Norway</option>
									<option value="Oman" >Oman</option>
									<option value="Pakistan" >Pakistan</option>
									<option value="Palau" >Palau</option>
									<option value="Panama" >Panama</option>
									<option value="Papua New Guinea" >Papua New Guinea</option>
									<option value="Paraguay" >Paraguay</option>
									<option value="Peru" >Peru</option>
									<option value="Philippines" >Philippines</option>
									<option value="Poland" >Poland</option>
									<option value="Portugal" >Portugal</option>
									<option value="Puerto Rico" >Puerto Rico</option>
									<option value="Qatar" >Qatar</option>
									<option value="Romania" >Romania</option>
									<option value="Russia" >Russia</option>
									<option value="Rwanda" >Rwanda</option>
									<option value="Saint Kitts and Nevis" >Saint Kitts and Nevis</option>
									<option value="Saint Lucia" >Saint Lucia</option>
									<option value="Saint Vincent and the Grenadines" >Saint Vincent and the Grenadines</option>
									<option value="Samoa" >Samoa</option>
									<option value="San Marino" >San Marino</option>
									<option value="Sao Tome and Principe" >Sao Tome and Principe</option>
									<option value="Saudi Arabia" >Saudi Arabia</option>
									<option value="Senegal" >Senegal</option>
									<option value="Serbia and Montenegro" >Serbia and Montenegro</option>
									<option value="Seychelles" >Seychelles</option>
									<option value="Sierra Leone" >Sierra Leone</option>
									<option value="Singapore" >Singapore</option>
									<option value="Slovakia" >Slovakia</option>
									<option value="Slovenia" >Slovenia</option>
									<option value="Solomon Islands" >Solomon Islands</option>
									<option value="Somalia" >Somalia</option>
									<option value="South Africa" >South Africa</option>
									<option value="Spain" >Spain</option>
									<option value="Sri Lanka" >Sri Lanka</option>
									<option value="Sudan" >Sudan</option>
									<option value="Suriname" >Suriname</option>
									<option value="Swaziland" >Swaziland</option>
									<option value="Sweden" >Sweden</option>
									<option value="Switzerland" >Switzerland</option>
									<option value="Syria" >Syria</option>
									<option value="Taiwan" >Taiwan</option>
									<option value="Tajikistan" >Tajikistan</option>
									<option value="Tanzania" >Tanzania</option>
									<option value="Thailand" >Thailand</option>
									<option value="Togo" >Togo</option>
									<option value="Tonga" >Tonga</option>
									<option value="Trinidad and Tobago" >Trinidad and Tobago</option>
									<option value="Tunisia" >Tunisia</option>
									<option value="Turkey" >Turkey</option>
									<option value="Turkmenistan" >Turkmenistan</option>
									<option value="Tuvalu" >Tuvalu</option>
									<option value="Uganda" >Uganda</option>
									<option value="Ukraine" >Ukraine</option>
									<option value="United Arab Emirates" >United Arab Emirates</option>
									<option value="United Kingdom" >United Kingdom</option>
									<option value="United States" >United States</option>
									<option value="Uruguay" >Uruguay</option>
									<option value="Uzbekistan" >Uzbekistan</option>
									<option value="Vanuatu" >Vanuatu</option>
									<option value="Vatican City" >Vatican City</option>
									<option value="Venezuela" >Venezuela</option>
									<option value="Vietnam" >Vietnam</option>
									<option value="Yemen" >Yemen</option>
									<option value="Zambia" >Zambia</option>
									<option value="Zimbabwe" >Zimbabwe</option>	
								</select>
							</td>
							<td><span id="address_countryValResult"> </span></td>
						</tr>
						<tr>
							 <td><label for="phone">Telefoonnummer *</label></td>
							 <td><input type="text" id="phone" name="phone" maxlength="50" size="30"></td>
							 <td><span id="phoneValResult"> </span></td>
						</tr>
						<tr>
							 <td><label for="email">Email Adres *</label></td>
							 <td><input type="text" id="email" name="email" maxlength="80" size="30"></td>		 
							 <td><span id="emailValResult"> </span></td>		 
						</tr>
						<tr>
							 <td><label for="password">Wachtwoord *</label></td>
							 <td><input type="password" id="password" name="password" size="30" maxlength="30"></td>
							 <td><span id="passwordValResult"> </span></td>
						</tr>
						<tr>
							<td colspan="2" style="text-align:center"><input type="submit" value="Verstuur"></td>
						</tr>
					</table>
				</form>
							<!-- Het javascript bestand voor de validatie laden -->
			<script src="js/register.js"></script>
			<?php
			} // sluit de if
			?>
		</main>
		<script src="lib/jquery/jquery.min.js"></script>
		<!-- link naar het javascript bestand dat de validatie uitvoert -->
		<script src="js/validate.js"></script>

	</body>
</html>
