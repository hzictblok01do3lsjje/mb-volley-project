<?php
// Sessions, Security and Authorization
include ('security.php');
//Verbinding maken met de database
	require_once 'db.php';
	$mysqli =  connectDB();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well"><h1>Wedstrijdschema</h1></div>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
				<?php 
					$sql = "SELECT * FROM SPEELWEEK";
					$resWeken = $mysqli->query($sql);
					if($resWeken->num_rows == 0) {
						echo '<div class="alert alert-warning" role="alert">'.
									'<i class="fa fa-exclamation-triangle"></i> Er zijn geen speelweken gevonden</div>';
					} else {
						$expanded = " in";
						while ($rowWeek = $resWeken->fetch_assoc()) { 
							$date = date("d F Y", strtotime($rowWeek['datum']));
							$panelID = 'heading'.$rowWeek['id'];
							$collapseID = 'collapse'.$rowWeek['id'];
							?>
							
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="<?php echo $panelID ?>">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $collapseID ?>" 
											aria-expanded="false" aria-controls="<?php echo $collapseID ?>">
											Speelweek <?php echo $rowWeek['id'].": ".$date ?>
										</a>
									</h4>
								</div>
								<div id="<?php echo $collapseID ?>" class="panel-collapse collapse<?php echo $expanded ?>" role="tabpanel" 
									aria-labelledby="<?php echo $panelID ?>">
									<div class="panel-body">
										<?php
											$sql = "SELECT * FROM wedstrijdschema_view WHERE speelweek = ".$rowWeek['id']." ORDER BY id ASC";
											$resWedstr = $mysqli->query($sql);
											if(!$resWedstr || $resWedstr->num_rows == 0) {
												echo '<div class="alert alert-info" role="alert">'.
															'<i class="fa fa-info-circle"></i> Er zijn geen wedstrijden gevonden</div>';
											} else {
										?>
										<table class="table table-condensed table-striped">
											<tr>
												<th class="col-sm-1">Tijd</th>
												<th class="col-sm-1">Veld</th>
												<th class="col-sm-1">Klas</th>
												<th class="col-sm-3">Team A</th>
												<th class="col-sm-3">Team B</th>
												<th class="col-sm-2">Scheidsrechter/teller</th>
												<th class="col-sm-1">Klas</th>
												<th></th>
											</tr>
											<?php
												while($rowWedstr = $resWedstr->fetch_assoc()) {
													echo "<tr>";
													echo '<td>'.$rowWedstr['tijd']."</td>";
													echo "<td>".$rowWedstr['veld']."</td>";
													echo "<td>".$rowWedstr['klasse']."</td>";
													echo "<td>".$rowWedstr['teama']."</td>";
													echo "<td>".$rowWedstr['teamb']."</td>";
													echo "<td>".$rowWedstr['teams']."</td>";
													echo "<td>".$rowWedstr['klasse2']."</td>";
													echo "<td>";
													if ($rowWedstr['uitslag']== 0 ||$rowWedstr['uitslag']== 1) {
														echo '<a href="invullenuitslag.php?wedstrijdid='.$rowWedstr['id'].'">Uitslag</a>';
													} 
													if ($rowWedstr['teama']== NULL) {
														echo '<a href="gamemanage.php?wedstrijdid='.$rowWedstr['id'].'">Aanmaken</a>';
													} 
													echo "</td>";
													echo "</tr>";
												}
											?>
										</table>
										<?php } // end if ?> 
									</div>
								</div>
							</div>

							<?php 
							$expanded = "";
						}
					}
				?>


		</main>
	</body>
</html>
 