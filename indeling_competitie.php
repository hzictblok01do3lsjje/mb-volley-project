<?php
// Sessions, Security and Authorization
include ('security.php');
	
//Verbinding maken met de database
	require_once 'db.php';
	$mysqli =  connectDB();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MBV Volley</title>
		<?php include 'head.html' ?>		
	</head>
	<body>
		<?php include 'header.php' ?>
		<main class="container">	
			<div class="well">
				<h1>Indeling Competitie</h1>
				<p>Bekijk hier welke teams er bij elkaar in een klasse zitten. Klik op een teamnaam om meer te weten te komen van dat team.</p>
			</div>
			<?php 
				$sqlklassen = "SELECT * FROM KLAS ORDER BY code";
				$resultklassen = $mysqli->query($sqlklassen);

				if ($resultklassen->num_rows > 0) {
				    // output data of each row
				    while($klasse = $resultklassen->fetch_assoc()) { ?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2><i class="fa fa-cubes"></i> <?php echo $klasse['naam'] ?></h2>
						</div>
						<table class="table table-striped">
							<?php 
								$klasseCode = $klasse['code'];
								$sqlteams = "SELECT * FROM TEAM WHERE klasse='".$klasseCode."'";
								$resultteams = $mysqli->query($sqlteams);
								if($resultteams->num_rows > 0) {
									while ($team = $resultteams->fetch_assoc()) {
										$link  = "<tr><td class='col-sm-1'>";
										$link .= '<i class="fa fa-users"></i></td><td class="col-sm-10">';
										$link .= '<a href="team.php?teamid='.$team['id'].'">';
										$link .=  $team['naam'].'</a></td></tr>';
										echo $link;
									}
								} else {
									echo '<div class="alert alert-warning" role="alert">'.
									'<i class="fa fa-exclamation-triangle"></i> Er zijn geen teams in deze klasse</div>';
								}
							?>
						</table>
						</div>
				    <?php }
				} else {
					echo '<div class="alert alert-warning" role="alert">'.
						'<i class="fa fa-exclamation-triangle"></i> Er zijn geen klassen in deze competitie</div>';
				}

			?>
		</main>
	</body>
</html>