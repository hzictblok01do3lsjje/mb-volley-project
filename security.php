<?php
	session_start(); 
    
	 /**
	  * Controleert of het request is gedaan door een geauthentiseerde gebruiker
	  * @return TRUE als gebruiker een geauthentiseerde gebruiker is, anders FALSE 
	  */
	 function isAuthenticated() {
		if (!isset($_SESSION)) {
			return false;
		}
	 	return isset($_SESSION['loggedin']);
	 }
	 
	 /**
	  * Retourneert de gebruikersnaam van de geauthentiseerde gebruiker, anders een 
	  * waarschuwingsbericht.
	  * @return e gebruikersnaam van de geauthentiseerde gebruiker, anders een 
	  * waarschuwingsbericht.
	  */
	 function getAuthenticatedUsername() {
	 	if(!isAuthenticated()) {
	 		return "<USER NOT AUTHENTICATED>";
	 	}
		return $_SESSION['username'];
	 }
	 
	
?>